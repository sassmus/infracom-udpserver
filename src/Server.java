import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Server {

	private DatagramSocket socket;

	public static BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

	static Logger logger = Logger.getLogger("Test");  


	public Server(int port) throws SocketException {
		socket = new DatagramSocket(port);
	}

	public static void main(String[] args) {
		
		FileHandler fh; 

		int port = 17;

		try {
			
			//LOGGER

			Date date = new Date();  
	        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH_MM_SS");
	        
	        SimpleDateFormat formatterFecha = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
	        
	        fh = new FileHandler( formatter.format(date) + ".log");  
	        logger.addHandler(fh);
	        
	        SimpleFormatter formatter2 = new SimpleFormatter();  
	        fh.setFormatter(formatter2);  
	        
	        logger.info("Prueba " + formatterFecha.format(date));
	        
	        //Servidor UDP
			
			Server server = new Server(port);
			System.out.println("Server running on port " + port);

			System.out.println("Seleccione el archivo que desea enviar.\n 1. Archivo de prueba(100 Mb).\n 2.Archivo de prueba (250 Mb).");
			int archivo = Integer.parseInt(console.readLine());
			server.service(archivo);

		} catch (SocketException ex) {
			System.out.println("Socket error: " + ex.getMessage());
		} catch (IOException ex) {
			System.out.println("I/O error: " + ex.getMessage());
		}
	}

	private void service(int archivo) throws IOException {
		int contadorCliente = 0;
		while (true) {

			String file = null;
			int size = 0; 
			if(archivo == 1) {
				file = "../data/cap1.pptx";
				size = 250821787;
			}
			if(archivo == 2) {
				file = "../data/cap2.pptx";
				size = 92705080;
			}
			if(archivo == 3) {
				file = "../data/prueba.txt";
				size = 100660;
			}
			
			
			DatagramPacket request = new DatagramPacket(new byte[1], 1);


			socket.receive(request);
			
			
			contadorCliente++;
			logger.info("Se conecto el cliente: " + contadorCliente);
			logger.info("Archivo: " + file);
			logger.info("Tamanho: " + size);
			
			String quote = "SIZE:" + size;
			byte[] buffer = quote.getBytes();

			InetAddress clientAddress = request.getAddress();
			int clientPort = request.getPort();

			DatagramPacket response = new DatagramPacket(buffer, buffer.length, clientAddress, clientPort);
			socket.send(response);
			
			
			FileInputStream fis = new FileInputStream(file);
			buffer = new byte[4096];

			int contador = 0;
    		int read = 0;
    		int enviados = 0;

			while (fis.read(buffer) > 0) {
				System.out.println("Enviados: " + ++contador);
				response = new DatagramPacket(buffer, buffer.length, clientAddress, clientPort);
                read = response.getLength();
                enviados ++;

				socket.send(response);
				quote = new String(buffer, 0, read);
			}
			logger.info("Numero de fragmentos enviados: " + enviados);
			
			DatagramPacket recibidos = new DatagramPacket(buffer, buffer.length, clientAddress, clientPort);
			socket.receive(recibidos);
			//int cantidadFragmentosRecididos = new Integer(recibidos);
			
//			read = recibidos.getLength();
//			Integer r = new Integer
//          quote = new Integer(buffer, 0, read);
            System.out.println("Quote " + quote);
			
			fis.close();


//			String quote = "ibdf iu gdfiou gfdoi gghfdoi";
//			byte[] buffer = quote.getBytes();
//
//			InetAddress clientAddress = request.getAddress();
//			int clientPort = request.getPort();
//
//			DatagramPacket response = new DatagramPacket(buffer, buffer.length, clientAddress, clientPort);
//			socket.send(response);
		}
	}

}
